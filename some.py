import os
from model.pspnet import SegNet 
import torch
from PIL import Image
import numpy as np
from util.util import AverageMeter , colorize ,intersectionAndUnionGPU
import util.transform as transform
from util.dataset import CustomData
import util.dataset as data
import cv2
import torch.nn.functional as F


value_scale = 255
mean = [0.485, 0.456, 0.406]
mean = [item * value_scale for item in mean]
std = [0.229, 0.224, 0.225]
std = [item * value_scale for item in std]



i_channel = 3
o_channel = 2
model = SegNet(i_channel , o_channel)

base_dir = os.getcwd() + '/snet/All_Data/test'

img_dir = base_dir + '/gt'
lab_dir = base_dir + '/images'
imgs = os.listdir(img_dir)
labels = os.listdir(lab_dir)

curr_dir =os.getcwd() + '/snet'
save_folder = os.path.join(curr_dir , 'save') 
if ( os.path.exists(save_folder) is False):
    os.mkdir(save_folder)


gray_folder = os.path.join(save_folder, 'gray')
if ( os.path.exists(gray_folder) is False):
    os.mkdir(gray_folder)
color_folder = os.path.join(save_folder, 'color')
if ( os.path.exists(color_folder) is False):
    os.mkdir(color_folder)
    
    
test_transform = transform.Compose([transform.ToTensor()])
test_data = CustomData(data_root_label=curr_dir + '/ICDAR2003/test/gt',
                       data_root_train=curr_dir + '/ICDAR2003/test/images', transform=test_transform , train=False)
colors_path = curr_dir + '/dataset/ade20k/colors.txt'
names_path=curr_dir +'/dataset/ade20k/names.txt'
test_loader = torch.utils.data.DataLoader(
    test_data, batch_size=1, shuffle=False, num_workers=2, pin_memory=True)
colors = np.loadtxt(colors_path).astype('uint8')
names = [line.rstrip('\n') for line in open(names_path)]

data_list = data.make_data(
    curr_dir +'/ICDAR2003/test/gt', curr_dir + '/ICDAR2003/test/images')







# img1 = img_dir + '/' + imgs[0]
# lab1 = lab_dir +'/' + labels[0]


# img1 = torch.tensor(np.asarray(Image.open(img1)))
# lab1 = torch.tensor(np.asarray(Image.open(lab1), dtype=np.uint8))
# img1 = img1.permute( 2 , 0, 1)
# img1 = img1.unsqueeze(0)
# print( img1.shape)
# print( lab1.shape)

model_path = os.getcwd() + '/snet/the_new_segnet.pth'
checkpoint = torch.load(model_path )
model.load_state_dict(checkpoint['state_dict'], strict=False)
model = model.cuda()

# img1 = img1.cuda()


# output , softmaxed_output = model(img1)
# print( output.shape)
# print( softmaxed_output.shape) 
# print(softmaxed_output[0,:,0,0].sum())

# _ , predicted = torch.max( softmaxed_output[0 , : , : , :]  , 0)
# predicted = predicted * 255


def test(test_loader, data_list, model, classes, mean, std, gray_folder, color_folder):
    data_time = AverageMeter()
    batch_time = AverageMeter()
    model.eval()
    for i, (input, _) in enumerate(test_loader):

        input = input.cuda()
        _ , sof_out = model( input)
        _ , predicted = torch.max ( sof_out[ 0 , : , : ,  :] , 0)
        gray = np.uint8(predicted.detach().cpu().numpy())
        predicted = predicted * 255
        color = predicted.detach().cpu().numpy()
        color = np.asarray( color , dtype = np.uint8)
        color = Image.fromarray( color )
        image_path, _ = data_list[i]
        h , w = Image.open( image_path ).size
        color = color.resize( ( h ,w))
        image_name = image_path.split('/')[-1].split('.')
        image_name ='.'.join( i for i in image_name[:-1])
        gray_path = os.path.join(gray_folder, image_name + '.png')
        color_path = os.path.join(color_folder, image_name + '.png')
        cv2.imwrite(gray_path, gray)
        color.save(color_path)
        
def cal_acc(data_list, pred_folder, classes, names):
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()
    target_meter = AverageMeter()

    for i, (image_path, target_path) in enumerate(data_list):
        image_name = image_path.split('/')[-1].split('.')
        image_name ='.'.join( i for i in image_name[:-1])
        pred = cv2.imread(os.path.join(
            pred_folder, image_name+'.png'), cv2.IMREAD_GRAYSCALE)
        target = cv2.imread(target_path, cv2.IMREAD_GRAYSCALE)
        print( target.shape)
        print( pred.shape)
        assert( target.shape == pred.shape)
        intersection, union, target = intersectionAndUnionGPU(
            pred, target, classes)
        intersection_meter.update(intersection)
        union_meter.update(union)
        target_meter.update(target)
        accuracy = sum(intersection_meter.val) / \
            (sum(target_meter.val) + 1e-10)
        # logger.info('Evaluating {0}/{1} on image {2}, accuracy {3:.4f}.'.format(
        #     i + 1, len(data_list), image_name+'.png', accuracy))

    iou_class = intersection_meter.sum / (union_meter.sum + 1e-10)
    accuracy_class = intersection_meter.sum / (target_meter.sum + 1e-10)
    mIoU = np.mean(iou_class)
    mAcc = np.mean(accuracy_class)
    allAcc = sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)

    for i in range(classes):
        print('Class_{} result: iou/accuracy {:.4f}/{:.4f}, name: {}.'.format(i,
                                                                                    iou_class[i], accuracy_class[i], names[i]))
    print(
        'Eval result: mIoU/mAcc/allAcc {:.4f}/{:.4f}/{:.4f}.'.format(mIoU, mAcc, allAcc))
    for i in range(classes):
        print('Class_{} result: iou/accuracy {:.4f}/{:.4f}, name: {}.'.format(i,iou_class[i], accuracy_class[i], names[i]))

        
        


test(test_loader, data_list, model, 2, mean, std, gray_folder, color_folder)
cal_acc( data_list , gray_folder , 2 , names)





        
        
        


        
    



