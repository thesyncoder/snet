from model.pspnet import get_model
from util.util import AverageMeter, intersectionAndUnionGPU
import time
import torch
import torch.nn.functional as F
import os
import util.transform as transform
import torchvision.transforms as transforms
from util.dataset import CustomData
import numpy as np
import matplotlib.pyplot as plt
import math
from PIL import Image

model = get_model(3, 2)
#print(model)
model =model.cuda()
# modules_ori = [model.layer0, model.layer1,
#                model.layer2, model.layer3, model.layer4]
# modules_new = [model.ppm, model.cls, model.aux]
# params_list = []
# for module in modules_ori:
#     params_list.append(dict(params=module.parameters(), lr=0.001))
# for module in modules_new:
#     params_list.append(dict(params=module.parameters(), lr=0.001 * 10))


start_lr = 1e-1
end_lr = 1e-7
optimizer = torch.optim.Adam( model.parameters()  , lr = 1e-6)
smoothing = 0.05
criterion = torch.nn.CrossEntropyLoss().cuda()


value_scale=255
mean=[0.485, 0.456, 0.406]
mean=[item * value_scale for item in mean]
std=[0.229, 0.224, 0.225]
std=[item * value_scale for item in std]
ignore_label =255

 

#getting the data
train_transform=transform.Compose([
            #transform.RandScale([0.5, 2.0]),
            # transform.RandRotate([-10, 10],
            #                      padding=mean, ignore_label=ignore_label),
            # transform.RandomGaussianBlur(),
            # transform.RandomHorizontalFlip(),
             #transform.Resize(473),
            # transform.Crop([473, 473], crop_type='rand',
            #              padding=mean, ignore_label=ignore_label),
            transform.ToTensor(),
            transform.Normalize(mean=mean, std=std)
            ])
data= CustomData(data_root_train=os.getcwd() + '/snet/All_Data/train/images',
                   data_root_label=os.getcwd() + '/snet/All_Data/train/gt',transform = train_transform )
train_loader = torch.utils.data.DataLoader(
    data, batch_size=8, shuffle=True, drop_last=True, num_workers=2 , pin_memory=True)







val_transform=transform.Compose([
            #transform.Resize(473) ,
            #transform.Crop([473, 473], crop_type='center',
            #              padding=mean, ignore_label=255),
            transform.ToTensor(),
            transform.Normalize(mean=mean, std=std)
            ])
val_data=CustomData(data_root_train=os.getcwd() + '/snet/All_Data/test/images' , data_root_label=os.getcwd() + '/snet/All_Data/test/gt' , transform = val_transform)
val_loader=torch.utils.data.DataLoader(
            val_data, batch_size=8, shuffle=False, num_workers=2, pin_memory=True)


import torch.nn as nn
def validate(val_loader, model, criterion=nn.CrossEntropyLoss(ignore_index=255)):
    batch_time=AverageMeter()
    data_time=AverageMeter()
    loss_meter=AverageMeter()
    intersection_meter=AverageMeter()
    union_meter=AverageMeter()
    target_meter=AverageMeter()
    model.eval()
    end=time.time()

    for i, (input, target) in enumerate(val_loader):
        data_time.update(time.time() - end)
        input=input.cuda()
        target=target.cuda()
        target = target.float()
        target = target / 255.0
        target = target.long()
        _ ,output=model(input)
        output = output.cuda()
        output=F.interpolate(output, size=target.size()[1:], mode='bilinear', align_corners=True)
        loss=criterion(output, target)
        
        
        n=input.size(0)
        loss=torch.mean(loss)
        output=output.max(1)[1]
        
        intersection, union, target=intersectionAndUnionGPU(
            output, target, 2)

        intersection, union, target=intersection.cpu(
        ).numpy(), union.cpu().numpy(), target.cpu().numpy()
        intersection_meter.update(intersection), union_meter.update(
            union), target_meter.update(target)

        accuracy=sum(intersection_meter.val) / (sum(target_meter.val) + 1e-10)
        loss_meter.update(loss.item(), n)
        
        batch_time.update(time.time() - end)
        end=time.time()
        

        iou_class=intersection_meter.sum / (union_meter.sum + 1e-10)
        accuracy_class=intersection_meter.sum / (target_meter.sum + 1e-10)
        mIoU=np.mean(iou_class)
        mAcc=np.mean(accuracy_class)
        allAcc=sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)


    return loss_meter.avg, mIoU, mAcc, allAcc , batch_time.sum


def train(train_loader, model, optimizer, epoch):
    global ii
    batch_time = AverageMeter()
    data_time = AverageMeter()
  
    loss_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()
    target_meter = AverageMeter()

    model.train()
    end = time.time()
    

    max_iter = 100 * len(train_loader)

    for i, (input, target) in enumerate(train_loader):
        data_time.update(time.time() - end)

        h = int((target.size()[1] - 1) / 8 *8 + 1)
        w = int((target.size()[2] - 1) / 8 *8 + 1)
        # 'nearest' mode doesn't support align_corners mode and 'bilinear' mode is fine for downsampling
        target = F.interpolate(target.unsqueeze(1).float(), size=(
            h, w), mode='bilinear', align_corners=True).squeeze(1).long()
        input = input.cuda()
        target = target.cuda()
        target = target.float()
        target = target / 255.0
        target = target.long()
        _ ,output = model(input)
        aux_weight = 0.4
        loss = criterion( output , target)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        loss = loss.float()
        output = output.cuda()
        loss=torch.mean(loss)
        output=output.max(1)[1]
        classes = 2
        n = input.size(0)
        intersection, union, target = intersectionAndUnionGPU(
            output, target, classes, 255)

        intersection, union, target = intersection.cpu(
        ).numpy(), union.cpu().numpy(), target.cpu().numpy()
        intersection_meter.update(intersection), union_meter.update(
            union), target_meter.update(target)
        accuracy = sum(intersection_meter.val) / \
            (sum(target_meter.val) + 1e-10)
        loss_meter.update(loss.item(), n)
        batch_time.update(time.time() - end)
        end = time.time()
        current_iter = epoch * len(train_loader) + i + 1
        iou_class = intersection_meter.sum / (union_meter.sum + 1e-10)
        accuracy_class = intersection_meter.sum / (target_meter.sum + 1e-10)
        mIoU = np.mean(iou_class)
        mAcc = np.mean(accuracy_class)
        allAcc = sum(intersection_meter.sum) / (sum(target_meter.sum) + 1e-10)
        
    return loss_meter.avg, mIoU, mAcc, allAcc , batch_time.sum / ( 1000 * 60)

save_path = os.getcwd() + '/snet'
save_file = os.path.join(save_path, 'loss.csv')

with open(save_file, 'a') as f:
    f.write('loss_train,mIoU_train, mAcc_train,allAcc_train,loss_val,mIoU_val, mAcc_val,allAcc_val')
    f.write('\n')
f.close()

no_epochs = 500
save_freq =20
## Define the  pwd

lt =[]
lv =[]
mt=[]
mv=[]
def cyclical_lr(stepsize, min_lr=3e-4, max_lr=3e-3):

    # Scaler: we can adapt this if we do not want the triangular CLR
    scaler = lambda x: 1.

    # Lambda function to calculate the LR
    lr_lambda = lambda it: min_lr + (max_lr - min_lr) * relative(it, stepsize)

    # Additional function to see where on the cycle we are
    def relative(it, stepsize):
        cycle = math.floor(1 + it / (2 * stepsize))
        x = abs(it / stepsize - 2 * cycle + 1)
        return max(0, (1 - x)) * scaler(cycle)

    return lr_lambda


lossfile = os.path.join(os.getcwd() +'/snet', 'loss.csv')
print('Training starts --------> ')
step_size = 4*len(train_loader)
# clr = cyclical_lr(step_size, min_lr=start_lr, max_lr=end_lr)
# scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, clr)
start_lr = 1e-1
end_lr = 1e-7
import math
# lr_lambda = lambda x: math.exp(x * math.log(end_lr / start_lr) / (no_epochs * len( train_loader)))
# scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda)


mx_IoU = 0
for epoch in range(no_epochs):
    epoch_log = epoch

    loss_val, mIoU_val, mAcc_val, allAcc_val , t_val=validate(
            val_loader, model)
    lv.append(loss_val)
    mv.append(mIoU_val)
    loss_train,mIoU_train, mAcc_train, allAcc_train , t_train=train(
        train_loader, model, optimizer, epoch)
    lt.append(loss_train)
    mt.append(mIoU_train)
    

    
       
    with open(save_file, 'a') as f:
        st = "{:6f},{:6f},{:6f},{:6f},{:6f},{:6f},{:6f},{:6f}".format(loss_train,mIoU_train, mAcc_train, allAcc_train,loss_val, mIoU_val, mAcc_val, allAcc_val )
        f.write(st)
        f.write("\n")   
    f.close()   
    print( 'Epoch {}/{} loss :{} valLoss :{} MeanIoU_train : {} MeanIou_val : {} Time_train :{} Time_val: {}'.format(epoch+1 ,no_epochs ,loss_train, loss_val ,mIoU_train , mIoU_val , t_train , t_val))
    if (epoch_log % save_freq == 0):
        filename=os.path.join(save_path, 'model') + '/train_epoch_' + str(epoch_log) + '.pth'
        torch.save({'epoch': epoch_log, 'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict()}, filename)
        #if epoch_log / save_freq > 2:
        #    deletename=save_path + '/train_epoch_' + \
        #    str(epoch_log - save_freq * 2) + '.pth'
        #    os.remove(deletename)
    if ( mx_IoU < mIoU_val):
        filename = os.path.join(save_path, 'the_') + \
            'new_segnet' + '.pth'
        torch.save({'epoch': epoch_log, 'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict()}, filename)
        mx_IoU = mIoU_val

    plt.plot(lt,color='r', label="training")
    plt.plot(lv,color='b', label="validation")
    plt.plot(mt,color='g' , label="Mean IoU train")
    plt.plot(mv,color='c' , label ="Mean IoU val")
# plt.plot(lr_find_lr, label="lr")
# plt.plot(lr_find_loss, label="loss")
	
    # plt.legend(loc="upper left")
    plt.savefig('plot_new.png')

plt.legend(loc="upper left")
        


